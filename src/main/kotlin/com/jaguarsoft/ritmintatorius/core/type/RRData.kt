package com.jaguarsoft.ritmintatorius.core.type

class RRData {
    var name: String? = null
    var id: String? = null
    var startTimeInMilliseconds: Long = 0
    var endTimeInMilliseconds: Long = 0
    var durationInMilliseconds: Long = 0
    var rrIntervals: List<Float> = emptyList()
    var extrasystoleIndices: List<Int> = emptyList()
    var temporaryDisorderBIndices: List<Int> = emptyList()
    var tachycardiaIndices: List<List<Int>> = emptyList()
    var bradycardiaIndices: List<List<Int>> = emptyList()
    var errorIndices: List<Int> = emptyList()
    var missingIndices: List<Int> = emptyList()
    var timeInSecondsForRrInterval: List<Int> = emptyList()
    var heartbeatAverages: List<Int> = emptyList()
}
