package com.jaguarsoft.ritmintatorius.core.type

class ConfidenceValues {
    var lowerBound: Double = 0.0
    var value: Double = 0.0
    var upperBound: Double = 0.0
}
