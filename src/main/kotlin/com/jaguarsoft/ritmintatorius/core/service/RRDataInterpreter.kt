package com.jaguarsoft.ritmintatorius.core.service

import com.jaguarsoft.ritmintatorius.core.type.RRData
import com.jaguarsoft.ritmintatorius.core.util.ResourceInitializer
import java.util.*

class RRDataInterpreter(
    private val missingIntervalService: MissingIntervalService = MissingIntervalService(),
    resourceInitializer: ResourceInitializer = ResourceInitializer()
) {

    var calcVariables: Properties = resourceInitializer.getProperty("variables.properties")

    fun analyzeRRData(rrData: RRData): RRData =
        with(rrData) {
            extrasystoleIndices = getExtrasystoles(rrIntervals)
            temporaryDisorderBIndices = getTemporaryDisordersB(rrIntervals)
            bradycardiaIndices = getBradycardias(rrIntervals)
            tachycardiaIndices = getTachycardias(rrIntervals)
            missingIndices = missingIntervalService.getFoundMissingIntervals(rrIntervals, timeInSecondsForRrInterval)
            return rrData
        }

    fun getExtrasystoles(rrIntervals: List<Float>) =
        mutableListOf<Int>().apply {
            val k = calcVariables.getProperty("K").toInt()
            var i = 2 + k
            while (i <= rrIntervals.size - k) {
                if (isExtrasystole(rrIntervals, i)) {
                    add(i - 2)
                    add(i - 1)
                    i++
                }
                i++
            }
        }

    fun getTemporaryDisordersB(rrIntervals: List<Float>) =
        mutableListOf<Int>().apply {
            for (i in 2 until rrIntervals.size)
                if (isTemporaryDisorderB(rrIntervals, i))
                    add(i - 1)
        }

    fun getTachycardias(rrIntervals: List<Float>): List<List<Int>> =
        mutableListOf<List<Int>>().apply {
            var i = 0
            while (i <= rrIntervals.size - 2) {
                val tachycardiaLength = getTachycardiaLength(rrIntervals, i)
                if (tachycardiaLength > 0) {
                    add(mutableListOf<Int>().apply {
                        add(i)
                        add(i + tachycardiaLength)
                    })
                    i += tachycardiaLength - 1
                }
                i++
            }
        }

    fun getBradycardias(rrIntervals: List<Float>): List<List<Int>> =
        mutableListOf<List<Int>>().apply {
            var i = 0
            while (i <= rrIntervals.size - 2) {
                val bradycardiaLength = getBradycardiaLength(rrIntervals, i)
                if (bradycardiaLength > 0) {
                    add(mutableListOf<Int>().apply {
                        add(i)
                        add(i + bradycardiaLength)
                    })
                    i += bradycardiaLength - 1
                }
                i++
            }
        }


    private fun isExtrasystole(rrIntervals: List<Float>, i: Int) =
        rrIntervals[i] != 0f && rrIntervals[i - 1] != 0f && rrIntervals[i - 2] != 0f && rrIntervals[i - 3] != 0f &&
            rrIntervals[i - 2] < calcNearbyMean(rrIntervals, i) && rrIntervals[i - 1] > calcNearbyMean(rrIntervals, i)

    private fun calcNearbyMean(rrIntervals: List<Float>, i: Int): Float {
        val k = calcVariables.getProperty("K").toInt()
        var elementSum = 0f
        for (KIter in 0 until k)
            elementSum += rrIntervals[i - 3 - KIter] + rrIntervals[i + KIter]
        return elementSum / (2 * k)
    }

    private fun isTemporaryDisorderB(rrIntervals: List<Float>, i: Int) =
        rrIntervals[i] != 0f && rrIntervals[i - 1] != 0f && rrIntervals[i - 2] != 0f &&
                calcVariables.getProperty("b").toFloat() * (rrIntervals[i - 2] + rrIntervals[i]) * 0.5f <= rrIntervals[i - 1]

    private fun getBradycardiaLength(rrIntervals: List<Float>, i: Int): Int {
        var timeSum = 0f
        var index = i
        var skippedIntervals = 0

        while (index < rrIntervals.size && timeSum + rrIntervals[index] <= 60f) {
            if (rrIntervals[index] == 0f)
                skippedIntervals++
            timeSum += rrIntervals[index]
            index++
        }

        if (timeSum <= 59f)
            return 0

        val length = index - i - skippedIntervals
        return if (timeSum / length.toFloat() >= 1f)
            length + skippedIntervals
        else 0
    }

    private fun getTachycardiaLength(rrIntervals: List<Float>, i: Int): Int {
        var timeSum = 0f
        var index = i
        var skippedIntervals = 0

        while (index < rrIntervals.size && timeSum + rrIntervals[index] <= 60f) {
            if (rrIntervals[index] == 0f)
                skippedIntervals++
            timeSum += rrIntervals[index]
            index++
        }

        if (timeSum <= 59f)
            return 0

        val length = index - i - skippedIntervals
        return if (timeSum / length.toFloat() <= 0.6)
            length + skippedIntervals
        else 0
    }

}
