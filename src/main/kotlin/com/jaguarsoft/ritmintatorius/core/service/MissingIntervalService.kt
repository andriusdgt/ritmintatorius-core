package com.jaguarsoft.ritmintatorius.core.service

import java.lang.Math.abs
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream

class MissingIntervalService {

    fun getFoundMissingIntervals(rr: List<Float>, elapsedTime: List<Int>): List<Int> =
        mutableListOf<Int>().apply {
            var startIndex = 0
            while (startIndex < rr.size) {
                val endIndex = initializeIndex(rr, startIndex)
                if (diffBetweenElapsedAndLoggedTime(rr, elapsedTime, startIndex, endIndex) > THRESHOLD_IN_SECONDS * 1000) {
                    add(getIndexOfLargestTimeGap(elapsedTime, startIndex, endIndex))
                    startIndex = endIndex
                }
                startIndex++
            }
        }

    private fun diffBetweenElapsedAndLoggedTime(
        rr: List<Float>, elapsedTime: List<Int>,
        startIndex: Int, endIndex: Int
    ) = abs(getElapsedTime(elapsedTime, startIndex, endIndex) - getLoggedTimeSum(rr, startIndex, endIndex))

    private fun getElapsedTime(elapsedTime: List<Int>, startIndex: Int, endIndex: Int) =
        (elapsedTime[endIndex] - elapsedTime[startIndex]) * 1000

    private fun getLoggedTimeSum(loggedTime: List<Float>, startIndex: Int, endIndex: Int) =
        getListSum(loggedTime.subList(startIndex, endIndex))

    private fun getListSum(list: List<Float>) =
        list
            .stream()
            .mapToInt({ it.toInt() })
            .sum()

    private fun getIndexOfLargestTimeGap(time: List<Int>, startIndex: Int, endIndex: Int) =
        getRange(startIndex + 1, endIndex)
            .stream()
            .max(Comparator.comparing<Int, Int> { i -> time[i!!] - time[i - 1] })
            .get()


    private fun getRange(start: Int, end: Int): List<Int> =
        IntStream
            .rangeClosed(start, end)
            .boxed()
            .collect(Collectors.toList())

    companion object {

        private const val THRESHOLD_IN_SECONDS = 4
        private const val ENTRY_CHECK_WINDOW_SIZE = 9

        private fun initializeIndex(rrIntervals: List<Float>, startIndex: Int): Int {
            var endIndex = startIndex + ENTRY_CHECK_WINDOW_SIZE
            if (endIndex >= rrIntervals.size)
                endIndex = rrIntervals.size - 1
            return endIndex
        }

    }

}
