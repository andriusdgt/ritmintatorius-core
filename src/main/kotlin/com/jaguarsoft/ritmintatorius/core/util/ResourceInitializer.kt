package com.jaguarsoft.ritmintatorius.core.util

import java.io.IOException
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class ResourceInitializer {

    private val LOGGER = Logger.getLogger(ResourceInitializer::class.java.name)
    private val PROPERTY_LOADER_LOAD_FAIL_MESSAGE = "Failed to load Spring property loader"

    fun getProperty(classPath: String): Properties {
        val stringsProperty = Properties()
        try {
            val input = ResourceInitializer::class.java.classLoader.getResourceAsStream(classPath)
            stringsProperty.load(input)
            input.close()
        } catch (e: IOException) {
            LOGGER.log(Level.SEVERE, PROPERTY_LOADER_LOAD_FAIL_MESSAGE, e)
            System.exit(1)
        }

        return stringsProperty
    }
}
