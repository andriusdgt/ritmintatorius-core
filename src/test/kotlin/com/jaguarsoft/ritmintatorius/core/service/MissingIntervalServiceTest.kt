package com.jaguarsoft.ritmintatorius.core.service

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MissingIntervalServiceTest {

    private lateinit var service: MissingIntervalService

    @BeforeEach
    fun setUp() {
        service = MissingIntervalService()
    }

    @Test
    fun detectsMissingIntervalsIfDifferenceBetweenMeasuredAndElapsedTimeIsMoreThan4Seconds() {
        val rr = listOf(1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000).map { it.toFloat() }
        val elapsedSeconds = listOf(1, 2, 4, 8, 9, 11, 12, 13, 14, 15)

        val data = service.getFoundMissingIntervals(rr, elapsedSeconds)

        assertFalse(data.isEmpty())
    }

    @Test
    fun marksMissingIntervalAtLargestGap() {
        val rr = listOf(1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000).map { it.toFloat() }
        val elapsedSeconds = listOf(1, 2, 4, 8, 9, 11, 12, 13, 14, 15)

        val data = service.getFoundMissingIntervals(rr, elapsedSeconds)

        assertEquals(3, data.iterator().next())
    }

    @Test
    fun doesNotFindMissingIntervals() {
        val rr = listOf(1000, 0, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000).map { it.toFloat() }
        val elapsedSeconds = listOf(1, 2, 7, 7, 7, 8, 8, 9, 10, 11)

        val data = service.getFoundMissingIntervals(rr, elapsedSeconds)

        assertTrue(data.isEmpty())
    }

    @Test
    fun producesSeveralMarksOnLongerLists() {
        val rr = listOf(
            1000, 0, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000,
            1000, 1000, 1000, 1000, 1000, 1000, 200, 600, 1400, 1000,
            1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000
        ).map { it.toFloat() }
        val elapsedSeconds = listOf(
            1, 2, 8, 8, 9, 10, 11, 12, 14, 15,
            16, 17, 18, 19, 20, 21, 26, 27, 28, 29,
            30, 31, 32, 33, 34, 35, 36, 37, 38, 40
        )

        val data = service.getFoundMissingIntervals(rr, elapsedSeconds)

        assertFalse(data.isEmpty())
        assertEquals(2, data[0])
        assertEquals(16, data[1])
    }

}