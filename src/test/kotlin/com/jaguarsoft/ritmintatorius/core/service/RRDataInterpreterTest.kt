package com.jaguarsoft.ritmintatorius.core.service

import com.jaguarsoft.ritmintatorius.core.type.RRData
import com.jaguarsoft.ritmintatorius.core.util.ResourceInitializer
import com.nhaarman.mockito_kotlin.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import java.util.*

class RRDataInterpreterTest {

    private lateinit var intervalService: MissingIntervalService

    private lateinit var rrDataInterpreter: RRDataInterpreter

    @BeforeEach
    fun setUp() {
        intervalService = mock()

        rrDataInterpreter = RRDataInterpreter(intervalService)
    }

    @Test
    fun `Gets analyzed RR Data`() {
        val analyzedRecording = rrDataInterpreter.analyzeRRData(RRData())

        assertNotNull(analyzedRecording)
        verify(intervalService).getFoundMissingIntervals(any(), any())
    }

    @Test
    fun `Integration test`() {
        val recording = rrDataInterpreter.analyzeRRData(
            RRData().apply {
                rrIntervals = listOf(
                    List(30) { 0.8f }, listOf(1.9f),
                    List(15) { 0.9f }, listOf(0.06f, 1.7f),
                    List(100) { 0.8f }, List(60) { 0.4f }, List(100) { 0.8f },
                    List(20) { 1.5f }, List(100) { 0.8f })
                    .flatten()
            })

        assertEquals(30, recording.temporaryDisorderBIndices[0])
        assertTrue(recording.extrasystoleIndices.contains(46))
        assertEquals(1, recording.tachycardiaIndices.size)
        assertEquals(1, recording.bradycardiaIndices.size)
    }

    @Nested
    inner class ExtrasystoleTests {

        @Test
        fun `Doesn't report any extrasystoles provided healthy rhythmogram`() {
            val extrasystoles = rrDataInterpreter
                .getExtrasystoles(listOf(0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f))

            assertTrue(extrasystoles.isEmpty())
        }

        @Test
        fun `Reports presence of extrasystoles`() {
            val extrasystoles = rrDataInterpreter
                .getExtrasystoles(listOf(0.9f, 0.9f, 0.9f, 0.85f, 0.06f, 1.7f, 0.9f, 0.95f, 0.9f))

            assertFalse(extrasystoles.isEmpty())
        }

        @Test
        fun `Contains correct indices of extrasystoles`() {
            val extrasystoles = rrDataInterpreter
                .getExtrasystoles(listOf(0.9f, 0.9f, 0.9f, 0.85f, 0.06f, 1.7f, 0.9f, 0.95f, 0.9f))

            assertEquals(4, extrasystoles[0])
            assertEquals(5, extrasystoles[1])
        }

        @Test
        fun `Reports multiple occurrences`() {
            val extrasystoles = rrDataInterpreter
                .getExtrasystoles(listOf(0.9f, 0.9f, 0.85f, 0.06f, 1.7f, 0.9f, 0.08f, 1.75f, 0.9f, 0.9f))

            assertEquals(3, extrasystoles[0])
            assertEquals(4, extrasystoles[1])
            assertEquals(6, extrasystoles[2])
            assertEquals(7, extrasystoles[3])
        }

        @Test
        fun `Calculations can be configured to calculate mean with more surrounding elements`() {
            val resourceInitStub: ResourceInitializer = mock()
            val propertiesSpy: Properties = mock()
            doReturn("0.2").`when`(propertiesSpy).getProperty("epsilon")
            doReturn("2").`when`(propertiesSpy).getProperty("K")
            doReturn(propertiesSpy).`when`(resourceInitStub).getProperty(any())
            rrDataInterpreter = RRDataInterpreter(intervalService, resourceInitStub)

            val extrasystoles =
                rrDataInterpreter.getExtrasystoles(listOf(1f, 1.5f, 0.5f, 0.5f, 1.5f, 0.5f, 1.5f, 1f, 1f, 1f))

            assertEquals(4, extrasystoles.size)
            assertEquals(3, extrasystoles[0])
            assertEquals(4, extrasystoles[1])
            assertEquals(5, extrasystoles[2])
            assertEquals(6, extrasystoles[3])
        }

        @Test
        fun `Fully scans rrIntervals`() {
            val resourceInitStub: ResourceInitializer = mock()
            val propertiesSpy: Properties = mock()
            doReturn("0.2").`when`(propertiesSpy).getProperty("epsilon")
            doReturn("1").`when`(propertiesSpy).getProperty("K")
            doReturn(propertiesSpy).`when`(resourceInitStub).getProperty(any())
            rrDataInterpreter = RRDataInterpreter(intervalService, resourceInitStub)

            rrDataInterpreter.getExtrasystoles(listOf(0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f))

            verify(propertiesSpy, times(4)).getProperty("K")
        }

        @Test
        fun `Scans less rrIntervals with bigger K value`() {
            val resourceInitStub: ResourceInitializer = mock()
            val propertiesSpy: Properties = mock()
            doReturn("3").`when`(propertiesSpy).getProperty("K")
            doReturn("0.2").`when`(propertiesSpy).getProperty("epsilon")
            doReturn(propertiesSpy).`when`(resourceInitStub).getProperty(any())
            rrDataInterpreter = RRDataInterpreter(intervalService, resourceInitStub)

            rrDataInterpreter.getExtrasystoles(listOf(0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f))

            verify(propertiesSpy, times(4)).getProperty("K")
        }

    }

    @Nested
    inner class BlockTests {

        @Test
        fun `Doesn't report any blocks provided healthy rhythmogram`() {
            val blocks = rrDataInterpreter
                .getTemporaryDisordersB(listOf(0.9f, 0.9f, 0.9f, 0.85f, 0.85f, 0.9f, 0.9f, 0.95f, 0.9f))

            assertTrue(blocks.isEmpty())
        }

        @Test
        fun `Reports presence of blocks`() {
            val blocks = rrDataInterpreter
                .getTemporaryDisordersB(listOf(0.9f, 0.9f, 0.9f, 0.85f, 0.85f, 0.9f, 2.0f, 0.95f, 0.9f))

            assertFalse(blocks.isEmpty())
        }

        @Test
        fun `Contains correct index of block`() {
            val blocks = rrDataInterpreter
                .getTemporaryDisordersB(listOf(0.9f, 0.9f, 0.9f, 0.85f, 0.85f, 0.9f, 2.0f, 0.95f, 0.9f))

            assertEquals(6, blocks[0])
        }

        @Test
        fun `Fully scans rrIntervals`() {
            val resourceInitStub: ResourceInitializer = mock()
            val propertiesSpy: Properties = mock()
            doReturn("2.1").`when`(propertiesSpy).getProperty("b")
            doReturn(propertiesSpy).`when`(resourceInitStub).getProperty(any())
            rrDataInterpreter = RRDataInterpreter(intervalService, resourceInitStub)

            rrDataInterpreter.getTemporaryDisordersB(listOf(0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f))

            verify(propertiesSpy, times(4)).getProperty("b")
        }
    }

    @Nested
    inner class BradycardiaTests {

        @Test
        fun `Doesn't report any Bradycardia provided healthy rhythmogram`() {
            val bradycardias = rrDataInterpreter.getBradycardias(List(120) { 0.9f })

            assertTrue(bradycardias.isEmpty())
        }

        @Test
        fun `Reports presence of Bradycardia`() {
            val bradycardias = rrDataInterpreter.getBradycardias(List(120) { 1.5f })

            assertFalse(bradycardias.isEmpty())
        }

        @Test
        fun `Reports two sequences of Bradycardia with correct indices`() {
            val bradycardias = rrDataInterpreter.getBradycardias(
                listOf(
                    List(100) { 0.9f },
                    List(20) { 1.5f },
                    List(100) { 0.9f })
                    .flatten().map { it }
            )

            assertEquals(2, bradycardias.size)
            assertEquals(51, bradycardias[0][0])
            assertEquals(110, bradycardias[0][1])
            assertEquals(110, bradycardias[1][0])
            assertEquals(169, bradycardias[1][1])
        }

        @Test
        fun `Finds Bradycardia at the end of sequence`() {
            val bradycardias = rrDataInterpreter.getBradycardias(
                listOf(
                    List(100) { 0.9f },
                    List(10) { 1.5f })
                    .flatten().map { it })

            assertEquals(1, bradycardias.size)
            assertEquals(51, bradycardias[0][0])
            assertEquals(110, bradycardias[0][1])
        }
    }

    @Nested
    inner class TachycardiaTests {

        @Test
        fun `Doesn't report any Tachycardia provided healthy rhythmogram`() {
            val tachycardias = rrDataInterpreter.getTachycardias(List(120) { 0.9f })

            assertTrue(tachycardias.isEmpty())
        }

        @Test
        fun `Reports presence of Tachycardia`() {
            val tachycardias = rrDataInterpreter.getTachycardias(List(120) { 0.5f })

            assertFalse(tachycardias.isEmpty())
        }

        @Test
        fun `Reports a sequence of Tachycardia with correct indices`() {
            val tachycardias = rrDataInterpreter.getTachycardias(
                listOf(
                    List(100) { 0.8f },
                    List(60) { 0.4f },
                    List(100) { 0.8f })
                    .flatten().map { it }
            )

            assertEquals(1, tachycardias.size)
            assertEquals(51, tachycardias[0][0])
            assertEquals(151, tachycardias[0][1])
        }

        @Test
        fun `Finds Tachycardia at the end of sequence`() {
            val tachycardias = rrDataInterpreter.getTachycardias(
                listOf(
                    List(100) { 0.8f },
                    List(50) { 0.4f })
                    .flatten().map { it })

            assertEquals(1, tachycardias.size)
            assertEquals(51, tachycardias[0][0])
            assertEquals(150, tachycardias[0][1])
        }
    }
}